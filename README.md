# Kendo grid column max width

Set a max width on a kendo grid column

Kendo grid columns support a width attribute. But they do not provide a max-width. Occassionally, you will have a grid where the contents of one column squeeze out the rest. Setting max-width on the wide column can help.

# Test in browser to see if it will work

```javascript
jQuery('th:nth-child(4), td:nth-child(4)', '.k-grid').css('max-width', '200px')
```

The above test targets the fourth column in the grid and shows that the grid responds as desired. Column #4 is reduced to 200 pixels wide.

# Example css

```css
.k-grid th:nth-child(4), 
.k-grid td:nth-child(4) {
    max-width: 200px;
}
```

